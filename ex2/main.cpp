#include <Windows.h>
#include <stdio.h>
#include <tchar.h>
#include <iostream>
#define FILENAME "gibrish.bin"

int main(int argc, TCHAR* argv[])
{
	if (argv[1] == NULL)
	{
		STARTUPINFO si;
		PROCESS_INFORMATION pi;

		ZeroMemory(&si, sizeof(si));
		si.cb = sizeof(si);
		ZeroMemory(&pi, sizeof(pi));

		HANDLE hFile;
		LPCSTR pFileName = FILENAME;

		//opens the gibrish file
		hFile = CreateFileA(
			pFileName,
			FILE_ALL_ACCESS,
			FILE_SHARE_READ | FILE_SHARE_WRITE,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			NULL);
		DWORD file_size = GetFileSize(hFile, NULL);

		//mapping the a part of the file to the ram
		HANDLE hMapFile;
		hMapFile = CreateFileMappingA(
			hFile,
			NULL,
			PAGE_EXECUTE_READWRITE,
			0,
			0,
			"Local\\myFile");

		if (hMapFile == NULL)
		{
			printf("HASN'T MAPPED!!!");
			system("pause");
		}

		//creating the process that will change the file
		CreateProcessA(NULL, "ex2 1", NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
		WaitForSingleObject(pi.hProcess, INFINITE);

		CloseHandle(hMapFile);
		CloseHandle(hFile);

		return 0;
	}
	else
	{
		BOOL success = FALSE;
		HANDLE hMapFile;

		//opening the file that was mapped in the first process
		hMapFile = OpenFileMappingA(
			FILE_MAP_WRITE,
			TRUE,
			"Local\\myFile");

		if (hMapFile == NULL)
		{
			printf("COULDN'T OPEN!!!");
			system("pause");
		}

		//the piece of the file will be in the process virtual memory
		LPTSTR pBuf;
		pBuf = (LPTSTR)MapViewOfFile(hMapFile,   // handle to map object
			FILE_MAP_ALL_ACCESS, // read/write permission
			0,
			0,
			1);

		pBuf[0] = 'Z'; //changes the memory

		UnmapViewOfFile(pBuf);
		CloseHandle(hMapFile);
	}
}